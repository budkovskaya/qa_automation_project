package homework11;

public abstract class EyesBrowsProducts implements MakeUpBagItem {

    public abstract void makeUpEyesBrows();

}
