package homework11;

public class BrowPencil extends EyesBrowsProducts {

    private String browPencilColor;
    private Brands browPencilBrand;
    private boolean isBrowPencilSharp;
    private final String PREFERRED_BROW_PENCIL_COLOR = "Brown";

    public String getBrowPencilColor() {
        return browPencilColor;
    }

    public void setBrowPencilColor(String browPencilColor) {
        this.browPencilColor = browPencilColor;
    }

    public Brands getBrowPencilBrand() {
        return browPencilBrand;
    }

    public void setBrowPencilBrand(Brands browPencilBrand) {
        this.browPencilBrand = browPencilBrand;
    }

    public boolean isBrowPencilSharp() {
        return isBrowPencilSharp;
    }

    public void setBrowPencilSharp(boolean browPencilSharp) {
        isBrowPencilSharp = browPencilSharp;
    }

    public String getPREFERRED_BROW_PENCIL_COLOR() {
        return PREFERRED_BROW_PENCIL_COLOR;
    }

    public BrowPencil(String browPencilColor, Brands browPencilBrand, boolean isBrowPencilSharp) {
        this.browPencilColor = browPencilColor;
        this.browPencilBrand = browPencilBrand;
        this.browPencilColor = browPencilColor;
    }

    public void sharpenPencil() {
        String result = (isBrowPencilSharp) ? "Brow Pencil " + getBrowPencilBrand() + " " + getBrowPencilColor() + " is sharp enough. You may use it." :
                "You need to sharpen " + getBrowPencilBrand() + " " + getBrowPencilColor() + " brow pencil before using it.";
        System.out.println(result);
    }

    @Override
    public void printItemName() {
        System.out.println("* Brow Pencil: brand - " + getBrowPencilBrand() + ", color - " + getBrowPencilColor() + ", is sharp - " + (isBrowPencilSharp ? "yes" : "no"));
    }

    @Override
    public void printItemColor(){
        System.out.println(getBrowPencilColor());
    }


    @Override
    public void printItemBrand() {
        System.out.println(getBrowPencilBrand());
    }

    @Override
    public void makeUpEyesBrows() {
        System.out.println("Brow pencil is on. You look much nicer!");
    }
}
