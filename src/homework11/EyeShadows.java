package homework11;

public class EyeShadows extends EyesBrowsProducts {

    private Brands eyeShadowsBrand;
    private String eyeShadowColor;
    private final String PREFERRED_EYE_SHADOWS_COLOR = "Shimmer";

    public String getEyeShadowColor() {
        return eyeShadowColor;
    }

    public void setEyeShadowColor(String eyeShadowColor) {
        this.eyeShadowColor = eyeShadowColor;
    }

    public Brands getEyeShadowsBrand() {
        return eyeShadowsBrand;
    }

    public void setEyeShadowsBrand(Brands eyeShadowsBrand) {
        this.eyeShadowsBrand = eyeShadowsBrand;
    }

    public String getPREFERRED_EYE_SHADOWS_COLOR() {
        return PREFERRED_EYE_SHADOWS_COLOR;
    }

    public EyeShadows(String eyeShadowColor, Brands eyeShadowsBrand) {
        this.eyeShadowColor = eyeShadowColor;
        this.eyeShadowsBrand = eyeShadowsBrand;
    }

    @Override
    public void printItemName() {
        System.out.println("* Eye Shadows: brand - " + getEyeShadowsBrand() + ", color - " + getEyeShadowColor());
    }

    @Override
    public void printItemColor() {
        System.out.println(getEyeShadowColor());
    }

    @Override
    public void printItemBrand() {
        System.out.println(getEyeShadowsBrand());
    }

    @Override
    public void makeUpEyesBrows() {
        System.out.println("Eye Shadows are on. You're eyes are really nice!");
    }
}
