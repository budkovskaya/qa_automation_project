package homework11;


import java.time.LocalDate;

public class Lipsticks extends LipProducts {

    private String lipstickColor;
    private boolean isLongLasting;
    private Brands lipstickBrand;
    private final String PREFERRED_LIPSTICK_COLOR = "Red";
    private LocalDate lipstickExpirationDate;

    public Brands getLipstickBrand() {
        return lipstickBrand;
    }

    public String getLipstickColor() {
        return lipstickColor;
    }

    public boolean isLongLasting() {
        return isLongLasting;
    }

    public void setLongLasting(boolean longLasting) {
        isLongLasting = longLasting;
    }

    public String getPREFERRED_LIPSTICK_COLOR() {
        return PREFERRED_LIPSTICK_COLOR;
    }

    public LocalDate getLipstickExpirationDate() {
        return lipstickExpirationDate;
    }

    public void setLipstickExpirationDate(LocalDate lipstickExpirationDate) {
        this.lipstickExpirationDate = lipstickExpirationDate;
    }

    // Create a class constructor
    public Lipsticks(String lipstickColor, boolean isLongLasting, Brands lipstickBrand) {
        this.lipstickColor = lipstickColor;
        this.isLongLasting = isLongLasting;
        this.lipstickBrand = lipstickBrand;
    }

    @Override
    public void printItemName() {
        System.out.println("* Lipstick: brand - " + getLipstickBrand() + ", color - " + getLipstickColor() + ", isLongLasting - " + (isLongLasting ? "yes" : "no"));
    }

    @Override
    public void printItemColor() {
        System.out.println(getLipstickColor());
    }

    // Overload method
    public void printItemColor(String LipstickColorForNewYear) {
        System.out.println("The best color of lipstick for New Year party is " + LipstickColorForNewYear);
    }

    @Override
    public void printItemBrand() {
        System.out.println(getLipstickBrand());
    }

    // Overload method
    public void printItemBrand(Brands favouriteBrand) {
        System.out.println("My favourite lipstick brand is " + favouriteBrand + ".");
    }

    @Override
    public void makeUpLips() {
        System.out.println("The lipstick is on! You look great!");
    }

}


