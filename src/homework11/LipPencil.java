package homework11;

public class LipPencil extends LipProducts {

    private String lipPencilColor;
    private Brands lipPencilBrand;
    private boolean isSharpEnough;

    public String getLipPencilColor() {
        return lipPencilColor;
    }

    public void setLipPencilColor(String lipPencilColor) {
        this.lipPencilColor = lipPencilColor;
    }

    public boolean isSharpEnough() {
        return isSharpEnough;
    }

    public void setSharpEnough(boolean sharpEnough) {
        isSharpEnough = sharpEnough;
    }

    public Brands getLipPencilBrand() {
        return lipPencilBrand;
    }

    public void setLipPencilBrand(Brands lipPencilBrand) {
        this.lipPencilBrand = lipPencilBrand;
    }

    private final String PREFERRED_LIP_PENCIL_COLOR = "Dark Red";

    public String getPREFERRED_LIP_PENCIL_COLOR() {
        return PREFERRED_LIP_PENCIL_COLOR;
    }

    public LipPencil(String lipPencilColor, Brands lipPencilBrand, boolean isSharpEnough) {
        this.lipPencilColor = lipPencilColor;
        this.lipPencilBrand = lipPencilBrand;
        this.isSharpEnough = isSharpEnough;
    }

    public void sharpenPencil() {
        String result = (isSharpEnough) ? "Lip Pencil " + getLipPencilBrand() + " " + getLipPencilColor() + " is sharp enough. You may use it." :
                "You need to sharpen " + getLipPencilBrand() + " " + getLipPencilColor() + " lip pencil before using it.";
        System.out.println(result);
    }

    @Override
    public void printItemName() {
        System.out.println("* Lip Pencil: brand - " + getLipPencilBrand() + ", color - " + getLipPencilColor());
    }

    @Override
    public void printItemColor() {
        System.out.println(getLipPencilColor());
    }

    @Override
    public void printItemBrand() {
        System.out.println(getLipPencilBrand());
    }

    @Override
    public void makeUpLips() {
        System.out.println("Lip pencil is on. Your lips look good!");
    }
}

