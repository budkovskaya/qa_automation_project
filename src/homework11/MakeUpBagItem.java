package homework11;

public interface MakeUpBagItem {

    public void printItemName();

    public void printItemColor();

    public void printItemBrand();

}
