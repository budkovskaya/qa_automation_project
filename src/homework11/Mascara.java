package homework11;

public class Mascara extends EyesBrowsProducts {

    private String mascaraColor;
    private Brands mascaraBrand;
    private MascaraPurpose mascaraPurpose;
    private final String PREFERRED_MASCARA_COLOR = "Black";

    public enum MascaraPurpose {
        VOLUMIZING,
        LIFTING,
        WATERPROOF,
        LENGTHENING
    }

    public String getMascaraColor() {
        return mascaraColor;
    }

    public void setMascaraColor(String mascaraColor) {
        this.mascaraColor = mascaraColor;
    }

    public Brands getMascaraBrand() {
        return mascaraBrand;
    }

    public void setMascaraBrand(Brands mascaraBrand) {
        this.mascaraBrand = mascaraBrand;
    }

    public MascaraPurpose getMascaraPurpose() {
        return mascaraPurpose;
    }

    public void setMascaraPurpose(MascaraPurpose mascaraPurpose) {
        this.mascaraPurpose = mascaraPurpose;
    }

    public String getPREFERRED_MASCARA_COLOR() {
        return PREFERRED_MASCARA_COLOR;
    }

    public Mascara(String mascaraColor, Brands mascaraBrand, MascaraPurpose mascaraPurpose) {
        this.mascaraColor = mascaraColor;
        this.mascaraBrand = mascaraBrand;
        this.mascaraPurpose = mascaraPurpose;
    }

    @Override
    public void printItemName() {
        System.out.println("* Mascara: brand - " + getMascaraBrand() + ", color - " + getMascaraColor() + ", purpose - " + getMascaraPurpose());
    }

    @Override
    public void printItemColor() {
        System.out.println(getMascaraColor());
    }

    @Override
    public void printItemBrand() {
        System.out.println(getMascaraBrand());
    }

    @Override
    public void makeUpEyesBrows() {
        System.out.println("The mascara is on. You're beautiful!");
    }

}
