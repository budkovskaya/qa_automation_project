package homework11;

import java.util.ArrayList;

public class Main {

    public static void main(String Args[]) throws Exception {

        // Create ArrayList for all available lipsticks objects
        ArrayList<Lipsticks> lipstickList = new ArrayList();

        // Create new lipsticks objects and add them to ArrayList with all available lipsticks
        lipstickList.add(new Lipsticks("Pink", true, Brands.ARTDECO));
        lipstickList.add(new Lipsticks("Red", true, Brands.COLLISTAR));
        lipstickList.add(new Lipsticks("Nude", false, Brands.GOSH));

        Lipsticks lipstick1 = new Lipsticks("Pink", true, Brands.ARTDECO);
        lipstick1.printItemColor("Red");
        lipstick1.printItemBrand(Brands.GOSH);

        System.out.println("The following lipstick colors are available: ");
        for (Lipsticks lipstick : lipstickList) {
            lipstick.printItemColor();
        }

        // Create ArrayList for all available lip pencils objects
        ArrayList<LipPencil> lipPencils = new ArrayList();

        // Create new lip pencils objects and add them to ArrayList with all available lip pencils
        lipPencils.add(new LipPencil("Dark Red", Brands.ARTDECO, false));
        lipPencils.add(new LipPencil("Brown", Brands.LOREAL, true));

        System.out.println("The following lip pencils colors are available: ");
        for (LipPencil lipPencil : lipPencils) {
            lipPencil.printItemColor();
        }
        for (LipPencil lipPencil : lipPencils) {
            lipPencil.sharpenPencil();
        }

        // Create ArrayList for all available eye shadows objects
        ArrayList<EyeShadows> eyeShadows = new ArrayList();

        // Create new eye shadows objects and add them to ArrayList with all available eye shadows
        eyeShadows.add(new EyeShadows("Nude", Brands.COLLISTAR));
        eyeShadows.add(new EyeShadows("Shimmer", Brands.ARTDECO));
        eyeShadows.add(new EyeShadows("Smoky Eyes", Brands.GOSH));

        System.out.println("The following eye shadows pallets are available: ");
        for (EyeShadows eyeShadow : eyeShadows) {
            eyeShadow.printItemColor();
        }

        // Create ArrayList for all available brow pencils objects
        ArrayList<BrowPencil> browPencils = new ArrayList<>();

        // Create new brow pencils objects and add them to ArrayList with all available brow pencils
        browPencils.add(new BrowPencil("Brown", Brands.LOREAL, true));
        browPencils.add(new BrowPencil("Dark Brown", Brands.BOURJOIS, false));

        System.out.println("The following brow pencils colors are available: ");
        for (BrowPencil browPencil : browPencils) {
            browPencil.printItemColor();
        }
        for (BrowPencil browPencil : browPencils) {
            browPencil.sharpenPencil();
        }

        // Create ArrayList for all available mascaras objects
        ArrayList<Mascara> mascaras = new ArrayList<>();

        // Create new mascara objects and add them to ArrayList with all available mascaras
        mascaras.add(new Mascara("Black", Brands.BOURJOIS, Mascara.MascaraPurpose.VOLUMIZING));
        mascaras.add(new Mascara("Black", Brands.GOSH, Mascara.MascaraPurpose.LENGTHENING));
        mascaras.add(new Mascara("Blue", Brands.LOREAL, Mascara.MascaraPurpose.WATERPROOF));

        System.out.println("The following mascara colors are available: ");
        for (Mascara mascara : mascaras) {
            mascara.printItemColor();
        }

        // Create an object for New Year MakeUp Bag
        NewYearMakeUpBag makeUpBag = new NewYearMakeUpBag();

        // Find the makeup item ot be added to NewYear MaKeUp bag
        makeUpBag.findLipstick(lipstickList);
        makeUpBag.findLipPencil(lipPencils);
        makeUpBag.findEyeShadows(eyeShadows);
        makeUpBag.findMascara(mascaras);
        makeUpBag.findBrowPencil(browPencils);


        // Print all items that were added to New Year MakeUp Bag
        makeUpBag.printAllMakeUpItems();

    }

}


