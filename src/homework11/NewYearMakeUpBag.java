package homework11;

import java.util.ArrayList;

public class NewYearMakeUpBag {

    // Create ArrayList for all items that will be added to New Year MakeUp Bag
    ArrayList<MakeUpBagItem> resultList = new ArrayList();


    // Choose needed lipstick that corresponds to special criteria and add it to New Year MakeUp Bag (ArrayList)
    public void findLipstick(ArrayList<Lipsticks> lipstickList) throws ItemNotFoundException {
        for (Lipsticks lipstick : lipstickList) {
            if (lipstick.isLongLasting() && lipstick.getLipstickColor() == lipstick.getPREFERRED_LIPSTICK_COLOR()) {
                System.out.println("The lipstick for New Year party is found and added to New Year MakeUp bag.");
                resultList.add(lipstick);
                return;
            }
        }
        throw new ItemNotFoundException ("Needed lipsticks weren't found. You need to buy a new one for a party.");
    }


    // Choose needed lip pencil that corresponds to special criteria and add it to New Year MakeUp Bag (ArrayList)
    public void findLipPencil(ArrayList<LipPencil> lipPencils) throws ItemNotFoundException {
        for (LipPencil lipPencil : lipPencils) {
            if (lipPencil.getLipPencilColor() == lipPencil.getPREFERRED_LIP_PENCIL_COLOR()) {
                System.out.println("The lip pencil for New Year party is found and added to New Year MakeUp bag.");
                resultList.add(lipPencil);
                return;
            }
        }
        throw new ItemNotFoundException("Needed lip pencils weren't found. You need to buy a new one for a party.");
    }

    // Choose needed eye shadows that correspond to special criteria and add it to New Year MakeUp Bag (ArrayList)
    public void findEyeShadows(ArrayList<EyeShadows> eyeShadows) throws ItemNotFoundException {
        for (EyeShadows eyeShadow : eyeShadows) {
            if (eyeShadow.getEyeShadowColor() == eyeShadow.getPREFERRED_EYE_SHADOWS_COLOR()) {
                System.out.println("The eye shadows for New Year party are found and added to New Year MakeUp bag.");
                resultList.add(eyeShadow);
                return;
            }
        }
        throw new ItemNotFoundException("Needed eye shadows weren't found. You need to buy a new one for a party.");
    }

    // Choose needed mascara that correspond to special criteria and add it to New Year MakeUp Bag (ArrayList)
    public void findMascara(ArrayList<Mascara> mascaras) throws ItemNotFoundException {
        for (Mascara mascara : mascaras) {
            if ((mascara.getMascaraColor() == mascara.getPREFERRED_MASCARA_COLOR()) && ((mascara.getMascaraPurpose() == Mascara.MascaraPurpose.VOLUMIZING))) {
                System.out.println("The mascara for New Year party is found and added to New Year MakeUp bag.");
                resultList.add(mascara);
                return;
            }
        }
        throw new ItemNotFoundException("Needed mascara wasn't found. You need to buy a new one for a party.");
    }

    // Choose needed mascara that correspond to special criteria and add it to New Year MakeUp Bag (ArrayList)
    public void findBrowPencil(ArrayList<BrowPencil> browPencils) throws ItemNotFoundException {
        for (BrowPencil browPencil : browPencils) {
            if ((browPencil.getBrowPencilColor() == browPencil.getPREFERRED_BROW_PENCIL_COLOR())) {
                System.out.println("The brow pencil for New Year party is found and added to New Year MakeUp bag.");
                resultList.add(browPencil);
                return;
            }
        }
        throw new ItemNotFoundException ("Needed brow pencils weren't found. You need to buy a new one for a party.");
    }


    // Print out all items that were chosen and added to New Year MakeUp Bag
    public void printAllMakeUpItems() {
        System.out.println("The New Year MakeUp bag is ready and contains the following items: ");
        for (MakeUpBagItem item : resultList) {
            item.printItemName();
        }
    }
}





