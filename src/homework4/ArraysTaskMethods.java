package homework4;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class ArraysTaskMethods {
    // Task 1
    // Method to generate random array
    public static int[] generateRandomArray(int arrayLength, int upperBound) {
        int[] array = new int[arrayLength];
        Random random = new Random();
        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(upperBound);
        }
        return array;
    }

    public static int findAverage(int[] array, int arrayLength) {
        int sum = 0;
        int averageSum = 0;
        for (int i = 0; i < array.length; i++) {
            sum += array[i];
            averageSum = sum / arrayLength;
        }
        return averageSum;
    }

    // Compare Average values of two arrays
    public static void compareAverage(int average1, int average2) {
        if (average1 > average2) {
            System.out.println("The average of the first array is bigger");
        } else if (average2 > average1) {
            System.out.println("The average of the second array is bigger");
        } else {
            System.out.println("The average of the two arrays are equal");
        }
    }

    /////////////////////////////////////////////////////////////////////////////////////
    // Task 2
    // Ask the user to enter the first number
    public static int[] createArray() {
        int[] array = new int[10];
        for (int i = 0; i < array.length; i++) {
            System.out.println("Please enter the number:");
            Scanner scanner = new Scanner(System.in);
            array[i] = scanner.nextInt();
        }
        return array;
    }

    ////////////////////////////////////////////////////////////////////////////////////////
    // Task 3
    public static void showMinMaxArrayValue() {
        // Create an array with length 8 and upper Bound 100. The method used is from the first task
        int[] array3 = generateRandomArray(8, 100);
        System.out.println("The array for third task is " + Arrays.toString(array3));

        // Sort array values
        Arrays.sort(array3);

        // Print min value of array
        int minValue = array3[0];
        System.out.println("Min value of an array is " + minValue);

        int maxValue = array3[array3.length - 1];
        System.out.println("Max value of an array is " + maxValue);
    }

    /////////////////////////////////////////////////////////////////////////////////////
    // Task 4
    public static void copyValuesToAnotherArray() {
        // Create and print array with length 10 and upper Bound 100. The method used is from the first task
        int[] array4 = generateRandomArray(10, 100);
        System.out.println("The array for fourth task is " + Arrays.toString(array4));

        // Create another array with the same length as first array for copying values here
        int[] anotherArray = new int[10];
        // Copy values from first array in reverse order to another array
        for (int i = 0; i < array4.length; i++) {
            anotherArray[i] = array4[array4.length - 1 - i];
        }
        System.out.println("The array with copied values in reverse order is " + Arrays.toString(anotherArray));
    }


}

