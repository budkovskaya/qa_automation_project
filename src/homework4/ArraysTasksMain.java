package homework4;

import java.util.Arrays;

public class ArraysTasksMain {
    public static void main(String[] args) {

        // Task 1
        // Generate and print the first array with 10 random numbers in range (0, 10)
        int[] array1 = ArraysTaskMethods.generateRandomArray(10, 11);
        System.out.println(Arrays.toString(array1));

        // Generate and print the second array with 10 random numbers in range (0, 10)
        int[] array2 = ArraysTaskMethods.generateRandomArray(10, 11);
        System.out.println(Arrays.toString(array2));

        // Calculate and print the average value of the first array
        int average1 = ArraysTaskMethods.findAverage(array1, 10);
        System.out.println(average1);

        // Calculate and print the average value of the second array
        int average2 = ArraysTaskMethods.findAverage(array2, 10);
        System.out.println(average2);

        // Compare the average values of two arrays and print the result
        ArraysTaskMethods.compareAverage(average1, average2);

        /////////////////////////////////////////////////////////////////////////////////////
        // Task 2
        // Save 10 user inputs in array
        int[] array = ArraysTaskMethods.createArray();

        // Print this array
        System.out.println("The array for second task is " + Arrays.toString(array));

        /////////////////////////////////////////////////////////////////////////////////////
        // Task 3
        ArraysTaskMethods.showMinMaxArrayValue();

        /////////////////////////////////////////////////////////////////////////////////////
        // Task 4
        ArraysTaskMethods.copyValuesToAnotherArray();
    }
}
