package homework10;

public class ExceedingWorkingHoursException extends Exception {

    public ExceedingWorkingHoursException(String message) {
        super(message);
    }
}
