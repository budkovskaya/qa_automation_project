package homework10;

public class Employee {

    private int employeeAge;
    private int employeeWorkingHours;

    public int getEmployeeAge() {
        return employeeAge;
    }

    public void setEmployeeAge(int employeeAge) {
        this.employeeAge = employeeAge;
    }

    public int getEmployeeWorkingHours() {
        return employeeWorkingHours;
    }

    public void setEmployeeWorkingHours(int employeeWorkingHours) {
        this.employeeWorkingHours = employeeWorkingHours;
    }

    public void hireEmployee(int employeeAge) throws IllegalWorkingAgeException {
        if (getEmployeeAge() >= 16){
            System.out.println("We can hire this employee. He is more than 16.");
        } else{
            throw new IllegalWorkingAgeException("It is illegal to hire this employee. He/she is under 16.");
        }
    }

    public void checkEmployeeWorkingHours (int employeeWorkingHours) throws ExceedingWorkingHoursException {
        if (getEmployeeWorkingHours() <= 40){
            System.out.println("Everything is fine. Employee has worked withing the limit of working hours.");
        } else {
            throw new ExceedingWorkingHoursException("Pay attention. The employee has worked more than 40 hours per week. It is illegal!");
        }
    }

}
