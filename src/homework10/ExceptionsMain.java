package homework10;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

public class ExceptionsMain {

    public static void main(String[] args) throws FileNotFoundException, IllegalWorkingAgeException {

        Employee employee1 = new Employee();
        employee1.setEmployeeAge(14);
        employee1.hireEmployee(employee1.getEmployeeAge());

        Employee employee2 = new Employee();
        employee2.setEmployeeWorkingHours(41);
        try {
            employee2.checkEmployeeWorkingHours(employee2.getEmployeeWorkingHours());
        } catch (ExceedingWorkingHoursException e) {
            e.printStackTrace();
        }


        File file = new File("C:\\Test\\new_file2.html");

        if (file.exists()) {
            throw new FileNotFoundException("File already exists. Please check your directory");
        } else {
            try {
                file.createNewFile();
                System.out.println("File is created");
                String name = file.getName();
                System.out.println("File name is " + name);
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                System.out.println("Work with files is finished");
            }
        }


    }
}
