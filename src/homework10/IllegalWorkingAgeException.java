package homework10;

public class IllegalWorkingAgeException extends Exception {

    public IllegalWorkingAgeException() {
    }

    public IllegalWorkingAgeException(String message) {
        super(message);
    }

    public IllegalWorkingAgeException(String message, Throwable cause) {
        super(message, cause);
    }
}
