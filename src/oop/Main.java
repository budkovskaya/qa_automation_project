package oop;

public class Main {
    public static void main(String[] args){
        Car car = new Car();
        car.setFuelInTank(10);
        car.setColor("grey");
        car.setFuelConsumption(30);

        car.fillUpTheTank(30);
        System.out.println("current fuel in the tank " + car.getFuelInTank());

        car.drive(15.5);
        System.out.println("needed fuel for 15.5 distance " + car.getFuelInTank());

        Car car1 = new Car("black", "sedan");
        car1.setFuelInTank(20);
        System.out.println("current fuel in the tank " + car1.getFuelInTank());




    }

}
