package oop;

public class Car {
    private String color;
    private String form;
    private double engineV;
    private double fuelConsumption;
    private String id;
    private double fuelInTank;


    public Car(){
    }

    public Car(String color, String form){
        this.color = color;
        this.form = form;
    }

    public Car(String color, String form, double engineV, double fuelConsumption, String id, double fuelInTank ){
        this.color = color;
        this.form = form;
        this.engineV = engineV;
        this.fuelConsumption = fuelConsumption;
        this.id = id;
        this.fuelInTank = fuelInTank;
    }
    //////////////////////////////////////////////////////////////////////////////////////////////////

    public String getColor(){
        return color;
    }

    public void setColor(String color){
        this.color = color;
    }

    public String getForm(){
        return form;
    }

    public void setForm(String form) {
        this.form = form;
    }

    public double getEngineV() {
        return engineV;
    }

    public void setEngineV(double engineV) {
        this.engineV = engineV;
    }

    public double getFuelConsumption() {
        return fuelConsumption;
    }

    public void setFuelConsumption(double fuelConsumption) {
        this.fuelConsumption = fuelConsumption;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public double getFuelInTank() {
        return fuelInTank;
    }

    public void setFuelInTank(double fuelInTank) {
        this.fuelInTank = fuelInTank;
    }

    //fill up the car
    void fillUpTheTank(int fuelVolume) {
        fuelInTank += fuelVolume;
    }

    // to travel a distance
    void drive(double distance) {
        fuelInTank = fuelInTank - distance / 100 * fuelConsumption;
    }

}

