package homework7;

public class Main {
    public static void main(String[] args) {

//        // Create new object with the first constructor, get and set values for this object and call method
////        Employee employee_1 = new Employee();
////        employee_1.setFirstName("Sasha");
////        employee_1.setLastName("Verbin");
////        employee_1.setHoursWorked(140);
////        employee_1.setRate(38);
////        double salary = employee_1.calculateSalary(employee_1.getRate(), employee_1.getHoursWorked());
////        System.out.println("The salary of " + employee_1.getFirstName() + " " + employee_1.getLastName() + " is " + salary + "$");
////
////
////        // Create new object with the second constructor, get and set values for this object and call method
////        Employee employee_2 = new Employee("Inna", "Shyshko");
////        employee_2.setRate(45.5);
////        employee_2.setHoursWorked(165);
////        double overtime = employee_2.calculateOvertime(employee_2.getHoursWorked(), employee_2.getRate());
////        System.out.println("Employee " + employee_2.getFirstName() + " " + employee_2.getLastName() + " will get " + overtime + "$ overtime this month");
////
////
////        // Create new object with the third constructor, get and set values for this object and call method
////        Employee employee_3 = new Employee("Lera", "Test", "QA");
////        employee_3.setRate(28);
////        employee_3.setHoursWorked(140);
////        double taxes = employee_3.calculateTaxes(employee_3.getRate(), employee_3.getHoursWorked());
////        System.out.println("Employee " + employee_3.getFirstName() + " " + employee_3.getLastName() + " should pay " + taxes + "$ of taxes this month");
////
////
////        // Create new object with the fourth constructor, get and set values for this object and call method
////        Employee employee_4 = new Employee("Alex", "Test", "Java developer", 30, "male", 30.00, 100, 3);
////        employee_4.displayEmployeeName();
////        employee_4.displayEmployeePosition();
////        employee_4.displayEmployeeAge();
////        int bonuses = employee_4.calculateBonuses(employee_4.getYearsWorkedInCompany());
////        System.out.println(employee_4.getFirstName() + " " + employee_4.getLastName() + " will get " + bonuses + "$ yearly bonuses.");
////
////
////        // Create new object with the fifth constructor, get and set values for this object and call method
////        Employee employee_5 = new Employee("Lena", "Ershova", "Office manager", 25, "female");
////        employee_5.setRate(25);
////        employee_5.setHoursWorked(150);
////        double salaryEmployee5 = employee_5.calculateSalary(employee_5.getRate(), employee_5.getHoursWorked());
////        System.out.println("Salary of Employee " + employee_5.getFirstName() + " " + employee_5.getLastName() + " is " + salaryEmployee5 + "$");
////        String holidayBonuses = employee_5.calculateHolidayBonuses(employee_5.getGender());
////        System.out.println(holidayBonuses);
////
////
////        // Create new object with the sixth constructor, set values for this object and call method
////        Employee employee_6 = new Employee("Dima", "Manin", 50.00);
////        employee_6.setPosition("QA");
////        employee_6.displayEmployeePosition();
////
////
////        // Create new object with the seventh constructor, get and set values for this object and call method
////        Employee employee_7 = new Employee("Anna", "Lakhachova", 35.50, 150);
////        employee_7.displayEmployeeHoursWorked();
////        double overtimeEmployee7 = employee_7.calculateOvertime(employee_7.getRate(), employee_7.getHoursWorked());
////        System.out.println(employee_7.getFirstName() + " " + employee_7.getLastName() + " will get " + overtimeEmployee7 + "$ for overtime");
////
////        // Create new object with the eighth constructor, get and set values for this object and call method
////        Employee employee_8 = new Employee("Alina", "Shvechova", "PM", 45.50);
////        employee_8.displayEmployeeRate();

        // Create new object for Developers class which extends from Employees, call inherited methods
        Developers developer_1 = new Developers("Anton", "Test");

        developer_1.setRate(25);
        developer_1.setHoursWorked(160);
        developer_1.setProgrammingLanguage("Java");

        // Call the override method
        double salaryDeveloper1 = developer_1.calculateSalary(developer_1.getRate(), developer_1.getHoursWorked());
        System.out.println("The salary of " + developer_1.getFirstName() + " " + developer_1.getLastName() + " is " + salaryDeveloper1 + "$");

        // Call the overload method
        developer_1.displayEmployeeName("Anton");

        // Call the new method in Developer class
        developer_1.displayDeveloperLanguage();

        // Create new object for QA class which extends from Employees, call inherited methods
        QA QA_1 = new QA("Inna", "Komarova", 20, 140);

        QA_1.setQASpeciality("Automation");
        QA_1.setRate(23);
        QA_1.setHoursWorked(145);

        // Call new method for class QA
        QA_1.displayQASpeciality();

        // Call the override method
        double taxes_QA_1 = QA_1.calculateTaxes(QA_1.getRate(), QA_1.getHoursWorked());
        System.out.println("The QA " + QA_1.getFirstName() + " " + QA_1.getLastName() + " will pay " + taxes_QA_1 + "$ of taxes");

        // Call the overload method
        QA_1.displayEmployeeAge(25);


        // Create new object for QA_Automation class which extends from QA, call inherited methods
        QA_Automation automation_QA_1 = new QA_Automation("Lera", "Budkovskaya", 20, 140, "DHL");

        // Call new method for Class QA_Automation
        automation_QA_1.displayQAAutomationProject();

        // Create new object for QA_Manual class which extends from QA, call inherited methods
        QA_Manual manual_QA_1 = new QA_Manual("Tanya", "Larionova", 18, 145, true);

        // Call new method for Class QA_Manual
        int bonuses_certification = manual_QA_1.setBonusesCertificationISTQB();
        System.out.println("QA " + manual_QA_1.getFirstName() + " " + manual_QA_1.getLastName() + " will get " + bonuses_certification + " additional yearly bonuses");

    }

}
