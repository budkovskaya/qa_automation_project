package homework7;

public class QA_Manual extends QA {

    public QA_Manual(String firstName, String lastName, double rate, double hoursWorked, boolean certificationISTQB) {
        super(firstName, lastName, rate, hoursWorked);
        this.certificationISTQB = certificationISTQB;
    }

    private boolean certificationISTQB;

    public boolean isCertificationISTQB() {
        return certificationISTQB;
    }

    public void setCertificationISTQB(boolean certificationISTQB) {
        this.certificationISTQB = certificationISTQB;
    }

    int setBonusesCertificationISTQB() {
        if (isCertificationISTQB()) {
            return 500;
        }
        return 300;
    }

}
