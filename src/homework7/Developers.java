package homework7;

public class Developers extends Employee {

    private String programmingLanguage;

    // create getter amd setter for new variable in class Developers
    public String getProgrammingLanguage() {
        return programmingLanguage;
    }

    public void setProgrammingLanguage(String programmingLanguage) {
        this.programmingLanguage = programmingLanguage;
    }

    // extend constructor from class Employee
    public Developers(String firstName, String lastName) {
        super(firstName, lastName);
    }

    // Override method
    @Override
    public double calculateSalary(double rate, double hoursWorked) {
        return rate * hoursWorked;
    }

    // Overload method
    public void displayEmployeeName(String name) {
        System.out.println("The employee name is " + name);
    }

    // New method for class Developers
    void displayDeveloperLanguage() {
        System.out.println("The developer " + getFirstName() + " " + getLastName() + " is writing in " + getProgrammingLanguage());
    }

}
