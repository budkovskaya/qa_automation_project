package homework7;

public class Employee {

    private String firstName;
    private String lastName;
    private String position;
    private int age;
    private String gender;
    private double rate;
    private double hoursWorked;
    private int yearsWorkedInCompany;

    //////////////////////Constructors//////////////////////////////////////////////////////////////////////////////////

    // Constructor #1
    public Employee() {
    }

    // Constructor #2
    public Employee(String firstName, String lastName) {
        this.setFirstName(firstName);
        this.setLastName(lastName);
    }

    // Constructor #3
    public Employee(String firstName, String lastName, String position) {
        this.setFirstName(firstName);
        this.setLastName(lastName);
        this.setPosition(position);
    }

    // Constructor #4
    public Employee(String firstName, String lastName, String position, int age, String gender, double rate, double hoursWorked, int yearsWorkedInCompany) {
        this.setFirstName(firstName);
        this.setLastName(lastName);
        this.setPosition(position);
        this.setAge(age);
        this.setGender(gender);
        this.setRate(rate);
        this.setYearsWorkedInCompany(yearsWorkedInCompany);
    }

    // Constructor #5
    public Employee(String firstName, String lastName, String position, int age, String gender) {
        this.setFirstName(firstName);
        this.setLastName(lastName);
        this.setPosition(position);
        this.setAge(age);
        this.setGender(gender);
    }


    // Constructor #6
    public Employee(String firstName, String lastName, double rate) {
        this.setFirstName(firstName);
        this.setLastName(lastName);
        this.setRate(rate);

    }

    // Constructor #7
    public Employee(String firstName, String lastName, double rate, double hoursWorked) {
        this.setFirstName(firstName);
        this.setLastName(lastName);
        this.setRate(rate);
        this.setHoursWorked(hoursWorked);
    }

    // Constructor #8
    public Employee(String firstName, String lastName, String position, double rate) {
        this.setFirstName(firstName);
        this.setLastName(lastName);
        this.position = position;
        this.setRate(rate);
    }

    /////////////////////////////////////////////Getters and Setters/////////////////////////////////////////////////////

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    public double getHoursWorked() {
        return hoursWorked;
    }

    public void setHoursWorked(double hoursWorked) {
        this.hoursWorked = hoursWorked;
    }

    public int getYearsWorkedInCompany() {
        return yearsWorkedInCompany;
    }

    public void setYearsWorkedInCompany(int yearsWorkedInCompany) {
        this.yearsWorkedInCompany = yearsWorkedInCompany;
    }

    //////////////////////////////////////////////Methods////////////////////////////////////////////////////////////////

    // Method 1: return salary
    public double calculateSalary(double rate, double hoursWorked) {
        double salary = rate * hoursWorked;
        return salary;
    }

    // Method 2: return Overtime
    public double calculateOvertime(double hoursWorked, double rate) {
        double standardHours = 160;
        if (hoursWorked > standardHours) {
            double overtime = (hoursWorked - standardHours) * rate * 2;
            return overtime;
        } else {
            return 0;
        }
    }

    // Method 3: return the sum how much taxes need to be paid
    public double calculateTaxes(double rate, double hoursWorked) {
        double taxRate = 0.05;
        double taxes = rate * hoursWorked * taxRate;
        return taxes;
    }

    // Method 4: calculate bonuses per year depending on how long employee works in the company
    public int calculateBonuses(int yearsWorkedInCompany) {
        if (yearsWorkedInCompany < 3) {
            return 0;
        } else if (yearsWorkedInCompany == 3) {
            return 500;
        } else if (yearsWorkedInCompany >= 4 && yearsWorkedInCompany <= 5) {
            return 800;
        } else {
            return 1000;
        }
    }

    // Method 5: calculate bonuses on holidays depending on gender
    public String calculateHolidayBonuses(String gender) {
        if (gender == "female") {
            return "All women will get additional 500$ on March 8";
        } else if (gender == "male") {
            return "Al men will get additional 500$ on 14th of October";
        } else {
            return "No additional bonuses";
        }
    }

    // Method 6: displays employee's name
    public void displayEmployeeName() {
        System.out.println("The employee name is " + getFirstName() + " " + getLastName());
    }

    // Method 7 - display employee's age
    public void displayEmployeeAge() {
        System.out.println("The age of employee " + getFirstName() + " " + getLastName() + " is " + getAge());
    }

    // Method 8 - display employee's position
    public void displayEmployeePosition() {
        System.out.println("The position of employee " + getFirstName() + " " + getLastName() + " is " + getPosition());
    }

    // Method 9 - display employee's rate
    public void displayEmployeeRate() {
        System.out.println("The rate of employee " + getFirstName() + " " + getLastName() + " is " + getRate());
    }

    // Method 10 - display how many hours employee has worked
    public void displayEmployeeHoursWorked() {
        System.out.println("The employee " + getFirstName() + " " + getLastName() + " has worked  " + getHoursWorked() + " hours this month");
    }


}
