package homework7;

public class QA extends Employee {

    private String QASpeciality;

    // create getter amd setter for new variable in class QA
    public String getQASpeciality() {
        return QASpeciality;
    }

    public void setQASpeciality(String QASpeciality) {
        this.QASpeciality = QASpeciality;
    }

    // extend constructor from class Employee
    public QA(String firstName, String lastName, double rate, double hoursWorked) {
        super(firstName, lastName, rate, hoursWorked);
    }

    // Override method
    @Override
    public double calculateTaxes(double rate, double hoursWorked) {
        double taxRate = 0.05;
        return rate * hoursWorked * taxRate;
    }

    // Overload method
    public void displayEmployeeAge(int age) {
        System.out.println("The age of employee " + getFirstName() + " " + getLastName() + " is " + age);
    }

    // New method for class QA
    void displayQASpeciality() {
        System.out.println("The QA " + getFirstName() + " " + getLastName() + " is " + getQASpeciality() + " QA");
    }

}
