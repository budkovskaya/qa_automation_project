package homework7;

public class QA_Automation extends QA {

    private String project;

    public QA_Automation(String firstName, String lastName, double rate, double hoursWorked, String project) {
        super(firstName, lastName, rate, hoursWorked);
        this.project = project;
    }

    public String getProject() {
        return project;
    }

    public void setProject(String project) {
        this.project = project;
    }

    // New method in QA_Automation Class
    void displayQAAutomationProject() {
        System.out.println("Automation QA " + getFirstName() + " " + getLastName() + " is working at " + getProject());
    }

}
