package homework3;

public class Warmup1Methods {

    //Task 1: sleepIn
    public static boolean sleepIn(boolean weekday, boolean vacation) {
        if (!weekday || vacation) {
            return true;
        }
        return false;
    }

    //Task 2: monkeyTrouble
    public static boolean monkeyTrouble(boolean aSmile, boolean bSmile) {
        if (aSmile && bSmile) {
            return true;
        } else if (!aSmile && !bSmile) {
            return true;
        }
        return false;
    }

    //Task 3: sumDouble
    public static int sumDouble(int a, int b) {
        if (a != b) {
            return a + b;
        } else {
            return ((a + b) * 2);
        }
    }

    //Task 4: diff21
    public static int diff21(int n) {
        if (n <= 21) {
            return Math.abs(n - 21);
        }
        return (Math.abs(n - 21) * 2);
    }

    //Task 5: parrotTrouble
    public static boolean parrotTrouble(boolean talking, int hour) {
        if (talking && (hour < 7 || hour > 20)) {
            return true;
        }
        return false;
    }

    //Task 6: makes10
    public static boolean makes10(int a, int b) {
        if (a == 10 || b == 10 || a + b == 10) {
            return true;
        }
        return false;
    }

    //Task 7: nearHundred
    public static boolean nearHundred(int n) {
        if ((Math.abs(n - 100) <= 10) || (Math.abs(n - 200) <= 10)) {
            return true;
        }
        return false;
    }

    //Task 8: posNeg
    public static boolean posNeg(int a, int b, boolean negative) {
        if (negative && (a < 0 && b < 0)) {
            return true;
        } else if (!negative && ((a < 0 && b > 0) || (a > 0 && b < 0))) {
            return true;
        } else {
            return false;
        }
    }

    //Task 9: notString
    public static String notString(String str) {
        if (str.startsWith("not")) {
            return str;
        }
        return ("not " + str);
    }

    //Task 10: missingChar
    public static String missingChar(String str, int n) {
        String str1 = str.substring(0, n);
        String str2 = str.substring(n + 1, str.length());
        return str1 + str2;
    }

    //Task 11: frontBack
    public static String frontBack(String str) {
        int stringLength = str.length();
        if (stringLength <= 1) {
            return str;
        }
        return str.charAt(stringLength - 1) + str.substring(1, stringLength - 1) + str.charAt(0);
    }

    //Task 12: front3
    public static String front3(String str) {
        if (str.length() >= 3) {
            String newStr = str.substring(0, 3);
            return newStr.concat(newStr + newStr);
        }
        return str.concat(str + str);
    }

    //Task 13: backAround:
    public static String backAround(String str) {
        String newStr = str.substring(str.length() - 1) + str.substring(0, str.length()) + str.substring(str.length() - 1);
        return newStr;
    }

    //Task 14: or35
    public static boolean or35(int n) {
        if ((n % 3 == 0) || (n % 5 == 0)) {
            return true;
        }
        return false;
    }

    //Task 15: front22
    public static String front22(String str) {
        if (str.length() < 2) {
            return str + str + str;
        }
        return str.substring(0, 2) + str.substring(0, str.length()) + str.substring(0, 2);
    }

    //Task 16: startHi
    public static boolean startHi(String str) {
        if (str.startsWith("hi")) {
            return true;
        }
        return false;
    }

    // Task 17: icyHot
    public static boolean icyHot(int temp1, int temp2) {
        if ((temp1 < 0 && temp2 > 100) || (temp1 > 100 && temp2 < 0)) {
            return true;
        }
        return false;
    }

    // Task 18: in1020
    public static boolean in1020(int a, int b) {
        if ((a >= 10 && a <= 20) || (b >= 10 && b <= 20)) {
            return true;
        }
        return false;
    }

    // Task 19: hasTeen
    public static boolean hasTeen(int a, int b, int c) {
        if ((a >= 13 && a <= 19) || (b >= 13 && b <= 19) || (c >= 13 && c <= 19)) {
            return true;
        }
        return false;
    }

    // Task 20: loneTeen
    public static boolean loneTeen(int a, int b) {
        boolean aNumberTeen = (a >= 13 && a <= 19);
        boolean bNumberTeen = (b >= 13 && b <= 19);
        if ((aNumberTeen && !bNumberTeen) || (!aNumberTeen && bNumberTeen)) {
            return true;
        }
        return false;
    }

    //Task 21: delDel
    public static String delDel(String str) {
        if ((str.length() > 4) && (str.substring(1, 4).equals("del"))) {
            return str.substring(0, 1) + str.substring(4, str.length());
        } else if ((str.length() == 4) && (str.substring(1, 4).equals("del"))) {
            return str.substring(0, 1);
        } else {
            return str;
        }
    }


    //Task 22: mixStart
    public static boolean mixStart(String str) {
        if (str.length() < 3) {
            return false;
        } else if (str.matches("(.*)ix(.*)")) {
            return true;
        } else {
            return false;
        }
    }

    //Task 23: startOz:
    public static String startOz(String str) {
        if ((str.length() == 1) && (str.charAt(0) == 'o')) {
            return str.substring(0, 1);
        } else if ((str.length() >= 2) && (str.charAt(0) == 'o') && (str.charAt(1) == 'z')) {
            return str.substring(0, 2);
        } else if ((str.length() >= 2) && !(str.charAt(0) == 'o') && (str.charAt(1) == 'z')) {
            return str.substring(1, 2);
        } else if ((str.length() >= 2) && (str.charAt(0) == 'o') && !(str.charAt(1) == 'z')) {
            return str.substring(0, 1);
        }
        return "";
    }


    // Task 24: intMax
    public static int intMax(int a, int b, int c) {
        int d = Math.max(a, b);
        int e = Math.max(d, c);
        return e;
    }

    // Task 25: close10
    public static int close10(int a, int b) {
        int a1 = Math.abs(a - 10);
        int b1 = Math.abs(b - 10);
        if (a1 == b1) {
            return 0;
        } else if (a1 < b1) {
            return a;
        }
        return b;
    }

    // Task 26: in3050
    public static boolean in3050(int a, int b) {
        if ((a >= 30 && a <= 40) && (b >= 30 && b <= 40) ||
                (a >= 40 && a <= 50) && (b >= 40 && b <= 50)) {
            return true;
        }
        return false;
    }

    // Task 27: max1020
    public static int max1020(int a, int b) {
        if ((a >= 10 && a <= 20) && (b >= 10 && b <= 20)) {
            return Math.max(a, b);
        } else if ((a >= 10 && a <= 20) && !(b >= 10 && b <= 20)) {
            return a;
        } else if (!(a >= 10 && a <= 20) && (b >= 10 && b <= 20)) {
            return b;
        }
        return 0;
    }

    //Task 28: stringE
    public static boolean stringE(String str) {
        int number = 0;
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == 'e') {
                number++;
            }
        }
        if (number >= 1 && number <= 3) {
            return true;
        }
        return false;
    }


    // Task 29: last Digit
    public static boolean lastDigit(int a, int b) {
        if (a % 10 == b % 10) {
            return true;
        }
        return false;
    }

    // Task 30: endUp
    public static String endUp(String str) {
        if (str.length() < 4) {
            return str.toUpperCase();
        }
        return str.substring(0, str.length() - 3) + str.substring(str.length() - 3).toUpperCase();
    }

}

