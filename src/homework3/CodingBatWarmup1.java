package homework3;

public class CodingBatWarmup1 {


    public static void main(String[] Args) {

        // Below the methods related to Warmup-1 exercise are called

        Warmup1Methods.sleepIn(true, false);

        Warmup1Methods.monkeyTrouble(false, false);

        Warmup1Methods.sumDouble(5, 6);

        Warmup1Methods.diff21(7);

        Warmup1Methods.parrotTrouble(true, 5);

        Warmup1Methods.makes10(2, 10);

        Warmup1Methods.nearHundred(-109);

        Warmup1Methods.posNeg(1, -1, false);

        Warmup1Methods.notString("candy");

        Warmup1Methods.missingChar("kitten", 3);

        Warmup1Methods.frontBack("code");

        Warmup1Methods.front3("Java");

        Warmup1Methods.backAround("cat");

        Warmup1Methods.or35(15);

        Warmup1Methods.front22("abc");

        Warmup1Methods.startHi("hi there");

        Warmup1Methods.icyHot(120, -1);

        Warmup1Methods.in1020(12, 99);

        Warmup1Methods.hasTeen(13, 19, 13);

        Warmup1Methods.loneTeen(13, 19);

        Warmup1Methods.delDel("adelbc");

        Warmup1Methods.mixStart("mix snacks");

        Warmup1Methods.startOz("ozymandias");

        Warmup1Methods.close10(8, 13);

        Warmup1Methods.in3050(30, 31);

        Warmup1Methods.max1020(11, 19);

        Warmup1Methods.stringE("Hello");

        Warmup1Methods.lastDigit(7, 17);

        Warmup1Methods.endUp("Hello");


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Below the methods related to String-1 exercise are called

        String1Methods.helloName("Bob");

        String1Methods.makeAbba("Hi", "Bye");

        String1Methods.makeTags("i", "Yay");

        String1Methods.makeOutWord("<<>>", "Yay");

        String1Methods.extraEnd("Hello");

        String1Methods.firstTwo("Hello");

        String1Methods.firstHalf("WooHoo");

        String1Methods.withoutEnd("Hello");

        String1Methods.comboString("Hello", "hi");

        String1Methods.nonStart("Hello", "There");

        String1Methods.left2("Hello");

        String1Methods.right2("Hello");

        String1Methods.theEnd("Hello", true);

        String1Methods.withouEnd2("Hello");

        String1Methods.middleTwo("string");

        String1Methods.endsLy("oddly");

        String1Methods.nTwice("Hello", 2);

        String1Methods.twoChar("java", 0);

        String1Methods.middleThree("Candy");

        String1Methods.hasBad("badxx");

        String1Methods.atFirst("hello");

        String1Methods.lastChars("last", "chars");

        String1Methods.conCat("abc", "cat");

        String1Methods.lastTwo("coding");

        String1Methods.seeColor("redxx");

        String1Methods.frontAgain("edited");

        String1Methods.minCat("Hello", "Hi");

        String1Methods.extraFront("Hello");

        String1Methods.withouEnd2("HelloHe");

        String1Methods.deFront("Hello");

        String1Methods.withoutX("xHix");

        String1Methods.withoutX2("xHi");

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Below the methods related to Logic-1 exercise are called

        Logic1Methods.cigarParty(30, false);

        Logic1Methods.dateFashion(5, 10);

        Logic1Methods.squirrelPlay(70, false);

        Logic1Methods.caughtSpeeding(60, false);

        Logic1Methods.sortaSum(3, 4);

        Logic1Methods.alarmClock(1, false);

        Logic1Methods.love6(6, 4);

        Logic1Methods.in1To10(5, false);


    }
}
