package homework3;

public class String1Methods {

    //Task1 : helloName
    public static String helloName(String name) {
        return ("Hello " + name + "!");
    }

    //Task 2: makeAbba
    public static String makeAbba(String a, String b) {
        return (a + b + b + a);
    }

    //Task 3: makeTags
    public static String makeTags(String tag, String word) {
        return "<" + tag + ">" + word + "<" + "/" + tag + ">";
    }

    //Task 4: makeOutWord
    public static String makeOutWord(String out, String word) {
        String firstPartOut = out.substring(0, 2);
        String secondPartOut = out.substring(2, 4);
        return firstPartOut + word + secondPartOut;
    }

    //Task 5: extraEnd
    public static String extraEnd(String str) {
        String lastTwoChars = str.substring(str.length() - 2, str.length());
        return lastTwoChars + lastTwoChars + lastTwoChars;
    }

    //Task 6: firstTwo
    public static String firstTwo(String str) {
        if (str.length() >= 2) {
            return str.substring(0, 2);
        }
        return str;
    }

    //Task 7: firstHalf
    public static String firstHalf(String str) {
        int halfOfString = str.length() / 2;
        return str.substring(0, halfOfString);
    }

    //Task 8: withoutEnd
    public static String withoutEnd(String str) {
        if (str.length() > 2) {
            return str.substring(1, str.length() - 1);
        }
        return "";
    }

    //Task 9: comboString
    public static String comboString(String a, String b) {
        if (a.length() > b.length()) {
            return b + a + b;
        }
        return a + b + a;
    }

    //Task 10: nonStart
    public static String nonStart(String a, String b) {
        return a.substring(1, a.length()) + b.substring(1, b.length());
    }

    //Task 11: left2
    public static String left2(String str) {
        if (str.length() > 2) {
            String firstTwoLetters = str.substring(0, 2);
            String otherStrPart = str.substring(2, str.length());
            return otherStrPart + firstTwoLetters;
        }
        return str;
    }

    //Task 13: right2
    public static String right2(String str) {
        if (str.length() > 2) {
            String lastTwoLetters = str.substring(str.length() - 2, str.length());
            String otherStrPart = str.substring(0, str.length() - 2);
            return lastTwoLetters + otherStrPart;
        }
        return str;
    }

    //Task 14: theEnd
    public static String theEnd(String str, boolean front) {
        if (front == true) {
            return str.substring(0, 1);
        }
        return str.substring(str.length() - 1, str.length());
    }

    //Task 15: withouEnd2
    public static String withouEnd2(String str) {
        if (str.length() > 2) {
            return str.substring(1, str.length() - 1);
        }
        return "";
    }

    //Task 16: middleTwo
    public static String middleTwo(String str) {
        int halfOfStrLength = str.length() / 2;
        if (str.length() > 2) {
            return str.substring(halfOfStrLength - 1, halfOfStrLength) +
                    str.substring(halfOfStrLength, halfOfStrLength + 1);
        }
        return str;
    }

    //Task 17: endsLy
    public static boolean endsLy(String str) {
        if ((str.length() >= 2) && (str.endsWith("ly"))) {
            return true;
        }
        return false;
    }

    //Task 18: nTwice
    public static String nTwice(String str, int n) {
        if (str.length() > n) {
            return str.substring(0, n) + str.substring(str.length() - n, str.length());
        } else {
            return str + str;
        }
    }

    //Task 19: twoChar
    public static String twoChar(String str, int index) {
        if (index < 0 || index >= str.length() - 1) {
            return str.substring(0, 2);
        }
        return str.substring(index, index + 2);
    }

    //Task 20: middleThree
    public static String middleThree(String str) {
        if (str.length() > 3) {
            int nearHalfStrLength = (str.length() - 1) / 2;
            return str.substring(nearHalfStrLength - 1, nearHalfStrLength + 2);
        }
        return str;
    }

    //Task21: hasBad
    public static boolean hasBad(String str) {
        if ((str.length() >= 3) && (str.substring(0, 3).equals("bad"))) {
            return true;
        } else if ((str.length() > 3) && (str.substring(1, 4).equals("bad"))) {
            return true;
        }
        return false;
    }

    //Task 22: atFirst
    public static String atFirst(String str) {
        if (str.length() == 1) {
            return str.substring(0, 1) + "@";
        } else if (str == "") {
            return "@@";
        } else {
            return str.substring(0, 2);
        }
    }

    //Task 23: lastChars
    public static String lastChars(String a, String b) {
        if (a.length() == 0 && b.length() > 0) {
            return "@" + b.substring(b.length() - 1, b.length());
        } else if (a.length() > 0 && b.length() == 0) {
            return a.substring(0, 1) + "@";
        } else if (a.length() == 0 && b.length() == 0) {
            return "@@";
        }
        return a.substring(0, 1) + b.substring(b.length() - 1, b.length());
    }

    //Task 24: conCat
    public static String conCat(String a, String b) {
        if (a.length() == 0) {
            return b;
        } else if (b.length() == 0) {
            return a;
        } else if (a.charAt(a.length() - 1) == b.charAt(0)) {
            return a + b.substring(1, b.length());
        }
        return a + b;
    }

    //Task 25: lastTwo
    public static String lastTwo(String str) {
        if (str.length() < 2) {
            return str;
        } else if (str.length() == 2) {
            return str.substring(1, 2) + str.substring(0, 1);
        } else {
            return (str.substring(0, str.length() - 2) + str.substring(str.length() - 1, str.length()) +
                    str.substring(str.length() - 2, str.length() - 1));
        }
    }

    //Task 26: seeColor
    public static String seeColor(String str) {
        if (str.startsWith("red")) {
            return "red";
        } else if (str.startsWith("blue")) {
            return "blue";
        }
        return "";
    }

    //Task 27: frontAgain
    public static boolean frontAgain(String str) {
        if (str.length() < 2) {
            return false;
        }
        if (str.length() >= 2) {
            String front = str.substring(0, 2);
            return (str.startsWith(front) == str.endsWith(front));
        }
        return false;
    }

    //Task 28: minCat
    public static String minCat(String a, String b) {
        if (a.length() > b.length()) {
            return a.substring(a.length() - b.length(), a.length()) + b;
        } else if (a.length() < b.length()) {
            return a + b.substring(b.length() - a.length(), b.length());
        } else {
            return a + b;
        }
    }

    //Task 29: extraFront
    public static String extraFront(String str) {
        if (str.length() <= 2) {
            return str + str + str;
        } else {
            String front = str.substring(0, 2);
            return front + front + front;
        }
    }

    //Task 30: without2
    public static String without2(String str) {
        if ((str.length() >= 3) && (str.substring(0, 2).equals(str.substring(str.length() - 2, str.length())))) {
            return str.substring(2, str.length());
        } else if (str.length() == 2) {
            return "";
        } else {
            return str;
        }
    }

    //Task 31: deFront
    public static String deFront(String str) {
        if ((str.length() >= 2) && (str.charAt(0) == 'a') && (str.charAt(1) == 'b')) {
            return str;
        } else if ((str.length() >= 2) && !(str.charAt(0) == 'a') && (str.charAt(1) == 'b')) {
            return str.substring(1, str.length());
        } else if ((str.length() >= 2) && (str.charAt(0) == 'a') && !(str.charAt(1) == 'b')) {
            return str.substring(0, 1) + str.substring(2, str.length());
        } else if ((str.length() >= 2) && !(str.charAt(0) == 'a') && !(str.charAt(1) == 'b')) {
            return str.substring(2, str.length());
        }
        return str;
    }

    //Task 32: startWord

    //Task 33: withoutX
    public static String withoutX(String str) {
        if (str == "x") {
            return "";
        } else if (str.startsWith("x") && str.endsWith("x")) {
            return str.substring(1, str.length() - 1);
        } else if (str.startsWith("x") && !(str.endsWith("x"))) {
            return str.substring(1, str.length());
        } else if (!(str.startsWith("x")) && (str.endsWith("x"))) {
            return str.substring(0, str.length() - 1);
        } else {
            return str;
        }
    }

    //Task 34: withoutX2
    public static String withoutX2(String str) {
        if ((str.length() == 1) && (str == "x")) {
            return "";
        } else if (str == "") {
            return str;
        } else if (str.startsWith("xx")) {
            return str.substring(2, str.length());
        } else if ((str.charAt(0) == 'x') && !(str.charAt(1) == 'x')) {
            return str.substring(1, str.length());
        } else if (!(str.charAt(0) == 'x') && (str.charAt(1) == 'x')) {
            return str.substring(0, 1) + str.substring(2, str.length());
        } else {
            return str;
        }
    }


}



