package homework3;

public class Logic1Methods {
    // Task 1: cigarParty
    public static boolean cigarParty(int cigars, boolean isWeekend) {
        if ((cigars >= 40 && cigars <= 60) && !(isWeekend)){
            return true;
        }else if (cigars >= 40 && isWeekend){
            return true;
        }else{
            return false;
        }
    }

    //Task 2: dateFashion
    public static int dateFashion(int you, int date) {
        if (you <= 2 || date <= 2){
            return 0;
        }else if (you >= 8 || date >= 8){
            return 2;
        }else{
            return 1;
        }
    }

    // Task 3: squirrelPlay
    public static boolean squirrelPlay(int temp, boolean isSummer) {
        if(temp >= 60 && temp <= 90 && !isSummer){
            return true;
        }else if(temp >= 60 && temp <= 100 && isSummer){
            return true;
        }else{
            return false;
        }
    }

    // Task 4: caughtSpeeding
    public static int caughtSpeeding(int speed, boolean isBirthday) {
        if ((speed <= 60 && !isBirthday) || (speed <= 65 && isBirthday)){
            return 0;
        }else if ((speed > 60 && speed <= 80 && !isBirthday) || (speed > 60 && speed <= 85 && isBirthday)){
            return 1;
        }else{
            return 2;
        }
    }

    // Task 5: sortaSum
    public static int sortaSum(int a, int b) {
        int sum = a+b;
        if (sum >= 10 && sum <= 19){
            return 20;
        }
        return sum;
    }

    // Task 6: alarmClock
    public static String alarmClock(int day, boolean vacation) {
        if (!(day == 0 || day == 6) && !(vacation)){
            return "7:00";
        }
        else if (((day == 0 || day == 6) && (!vacation)) || (!(day == 0 || day == 6) && (vacation))){
            return "10:00";
        } else {
            return "off";
        }
    }

    // Task 7: lave6
    public static boolean love6(int a, int b) {
        if ((a == 6) || (b == 6) || ((a + b) == 6) || (Math.abs(a - b) == 6)){
            return true;
        }
        return false;
    }

    // Task 8: in1To10
    public static boolean in1To10(int n, boolean outsideMode) {
        if ((n >=1 && n <= 10) && !outsideMode){
            return true;
        } else if ((n <=1 || n>= 10) && outsideMode){
            return true;
        }
        return false;
    }


}
