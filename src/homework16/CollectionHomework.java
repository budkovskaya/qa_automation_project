package homework16;

import java.util.*;

public class CollectionHomework {

    public static void main(String args[]) throws Exception {

        /* Create a new list of random numbers.
           List size can be set in variable desiredArraySize
           The function generateRandomIntRange is used to create a collection of random numbers.
           It allows to set in the parameter list the min and max values of numbers in collection.
         */
        int desiredArraySire = 20;
        List<Integer> numberList = new ArrayList<Integer>();
        for (int i = 0; i < desiredArraySire; i++) {
            numberList.add(generateRandomIntRange(-100, 100));
        }

        // Print out the first list with random numbers
        System.out.println("The number list consists of " + numberList);

        // Create and print out a new collection which will contain all numbers from the first collection
        Object cloneNumberList;
        cloneNumberList = ((ArrayList<Integer>) numberList).clone();
        System.out.println("The copy of first number list is " + numberList);

        // Define the unique numbers in collection
        // The list is converted to set, as it does not contains duplicate elements
        Set<Integer> setWithoutDuplicateValues = new LinkedHashSet<Integer>();
        setWithoutDuplicateValues.addAll(numberList);
        System.out.println("The collection without duplicate values: " + setWithoutDuplicateValues);

        // Define the quantity of uniques elements in collection
        System.out.println("The overall number of elements in collection is " + numberList.size());
        System.out.println("The overall number of unique elements in collection is " + setWithoutDuplicateValues.size());

        // Sort and print out the collection
        Collections.sort(numberList);
        System.out.println("Here is the sorted number list" + numberList);

        // Find min number in collection
        Collections.sort(numberList);
        System.out.println("The min number in collection is " + numberList.get(0));

        // Find max number in collection
        Collections.sort(numberList);
        System.out.println("The max number in collection is " + numberList.get(numberList.size() - 1));

        // Print out the sum of all numbers is collection
        int sum = 0;
        for (int i : numberList) {
            sum += i;
        }
        System.out.println("The sum of all numbers in the collection will be: " + sum);

        // Create new collection that contains only positive numbers from the first collection
        List<Integer> positiveNumberList = new ArrayList<Integer>();
        for (int i : numberList) {
            if (i > 0) {
                positiveNumberList.add(i);
            }
        }
        System.out.println("The new collection that contains only positive numbers from the first collection is " + positiveNumberList);

        // Delete from the first collection all odd numbers
        List<Integer> evenNumberList = new ArrayList<Integer>();
        for (int i : numberList) {
            if (!(i % 2 == 0)) {
                evenNumberList.add(i);
            }
        }
        System.out.println("The list that does not contain odd numbers is " + evenNumberList);

        // Find the defined number 7 in collection. If not found, return error message
        for (int i : numberList) {
            if (i == 7) {
                System.out.println("The number 7 is found in number collection.");
                ;
            } else {
                throw new Exception("Number 7 is not found in the given collection.");
            }
        } тут нужно сделать не конкретно под 7,а под любое число, что если я его передаю, то проверять валидное оно или нет.
        

    }

    public static int generateRandomIntRange(int min, int max) {
        Random random = new Random();
        return random.nextInt((max - min) + 1) + min;
    }

}
