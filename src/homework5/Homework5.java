package homework5;

import java.util.Scanner;

public class Homework5 {

    public static void main(String[] Args) {

        // Task 1: Do/while

        int num = 3;
        do {
            System.out.print(num + " ");
            num += 6;
        } while (num <= 69);
        System.out.print("\n");


        // Task 1: For loop

        for (int i = 3; i <= 69; i += 6) {
            System.out.print(i + " ");
        }
        System.out.print("\n");

        // Task 2a

        String str = "*";
        for (int i = 0; i <= 4; i++) {
            for (int a = 0; a < i; a++) {
                System.out.print(str);
            }
            System.out.print("\n");
        }
        System.out.print("\n");

        // Task 2b

        System.out.println("Please enter the grade:");
        Scanner scanner = new Scanner(System.in);
        int grade = scanner.nextInt();
        if (grade < 25) {
            System.out.println('F');
        } else if (grade >= 25 && grade < 45) {
            System.out.println('E');
        } else if (grade >= 45 && grade <= 50) {
            System.out.println('D');
        } else if (grade > 50 && grade <= 60) {
            System.out.println('C');
        } else if (grade > 60 && grade <= 80) {
            System.out.println('B');
        } else {
            System.out.println('A');
        }

        // Task 3
        System.out.println("Please enter the sum:");
        int sum = scanner.nextInt();
        switch (sum) {
            case 202:
                System.out.println("Не хватает 2 grn");
                break;
            case 200:
                System.out.println("Счастливчик");
                break;
            case 100:
                System.out.println("Пойду возьму еще что-то");
                break;
            default:
                System.out.println("Что-то пошло не так");
        }
    }
}
